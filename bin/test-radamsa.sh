#!/bin/bash

PID=$$
SAMPLES=radamsa-test-$PID

notify() {
   echo "$(date +'%Y.%m.%d %H:%M') $(hostname): $@" | nc -q 0 80.75.99.115 31337
}

failure() {
   notify "FAILURE: $@"
   echo "FAILURE: $@"
   exit 1
}

echo "Checking tools"
blab -e '42 10'
ol -e '(= 4 (+ 2 2))'

#echo "Compiling radamsa.c with ASan unless radamsa-asan exists"
#test -x radamsa-asan || asan -O2 -o radamsa-asan radamsa.c

rm fail-$PID
ROUND=1

while true
do
   notify "Radamsa big test round $ROUND"

   rm -rf $SAMPLES
   mkdir -p $SAMPLES/.new

   blab -e "[<>\"a ']{4096,}" \
      -n 5 -v -o $SAMPLES/charsoup-%n.txt

   blab -e '(\("<" | ">" | 0 | 10 | "0" | "a"*\)\1{100,}){10,}' \
      -n 5 -v -o $SAMPLES/charreps-%n.txt

   blab -e "([0-9. a] | [0-9]*){4096,}" \
      -n 5 -v -o $SAMPLES/numsoup-%n.txt

   blab -e 'o = x{100,}  n = [a-z]{2,4} x = [A-Z ]{0,10} | "<" \(n\) (" " (n ("=" a)))*">" x* "</" \1 ">" a = n | 34 n 34 | 39 n 39' \
      -n 20 -v -o $SAMPLES/blab-tagsoup-%n.xml

   touch $SAMPLES/empty

   for size in 1 2 4 8 16 32 64 128 256 512
   do
      NAME=rand-$size
      echo $NAME
      dd if=/dev/urandom bs=1K count=$size of=$SAMPLES/$NAME
      cat $SAMPLES/$NAME $SAMPLES/$NAME > $SAMPLES/$NAME.2
      cat $SAMPLES/$NAME.2 $SAMPLES/$NAME.2 > $SAMPLES/$NAME.4
   done

   for foo in $(seq 30)
   do
      echo "many $foo"
      ls $SAMPLES | sort -R | head -n 4 | while read path; do cat $SAMPLES/$path; done > $SAMPLES/many-$foo; 
   done

   for round in $(seq 1000)
   do
      echo "Test round $round at $(date)"
      (ulimit -S -t 100; radamsa -o $SAMPLES/.new/radamsa-%n --meta radamsa-$PID.log -n 100 -v $SAMPLES/* &> radamsa-$PID.out; REXIT=$?; test $REXIT -gt 0 && echo $REXIT > fail-$PID;)
      test -f fail-$PID && failure ">>>>>>>>>>>>>> radamsa test $PID failed <<<<<<<<<<<<<<<<<<<<" 
      rm $SAMPLES/radamsa-*
      ls $SAMPLES/.new/radamsa-* | wc -l | grep 100 || failure ">>>>>>> radamsa test $PID made too few files <<<<<<<<"
      mv $SAMPLES/.new/* $SAMPLES
   done

   ROUND=$(ol -e "(+ $ROUND 1)")
done
