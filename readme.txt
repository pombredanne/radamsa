INSTALLATION
  $ tar -zxvf radamsa-0.4.tar.gz
  $ cd radamsa-0.4
  $ make
  $ sudo make install

  You can also just copy radamsa-0.4/bin/radamsa somewhere (typically 
  to $HOME/bin) after building it.

  You need to get the Owl Lisp compiler in case you want to make
  changes to the actual source code, as opposed to the bundled pre-built
  C-code. This can be done with $ make get-owl.

  If your C compiler takes too much memory to compile radamsa, you can 
  change OFLAGS in Makefile to -O1.

UNINSTALLATION
   $ sudo make uninstall

REQUIREMENTS
  make
  a C-compiler (typically GCC or Clang)

SUPPORTED OPERATING SYSTEMS
  Linux
  OpenBSD
  Mac OS X

  A Windows-binary may be available at
  http://code.google.com/p/ouspg/downloads/list
